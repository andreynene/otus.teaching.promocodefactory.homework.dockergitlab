﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var assemblyName = typeof(DataContextFactory).GetTypeInfo().Namespace;
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();

            optionsBuilder
                .UseNpgsql("Host=localhost:5400;Database=docker;Username=docker;Password=habraqua",
                    x => x.MigrationsAssembly(assemblyName));

            return new DataContext(optionsBuilder.Options);
        }
    }

}